# test
This should be a scaffold project, so i can fork from this and have a basic auth ready.
Amplify will have to be set up from scratch though, 

by running amplify init (given that cli is installed and amplify configure has been run to create an iam user) and then amplify add auth


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

import Vue from "vue";
import Amplify, * as AmplifyModules from 'aws-amplify'
import { AmplifyPlugin } from 'aws-amplify-vue'
import App from "./App.vue";
import router from "./router";
import config from './aws-exports'
import store from "./store";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import './custom.scss'

Amplify.configure(config)

Vue.config.productionTip = false;
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(AmplifyPlugin, AmplifyModules)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
